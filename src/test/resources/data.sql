CREATE TABLE todo (
    id  BIGINT AUTO_INCREMENT PRIMARY KEY,
    username varchar(255),
    description varchar(255),
    target_date datetime(6),
    is_done TINYINT(1)
);

INSERT INTO todo(id, username, description, target_date, is_done)
VALUES(10001, 'Jacqueline', 'Test Todo 1', sysdate(), false);

INSERT INTO todo(id, username, description, target_date, is_done)
VALUES(10002, 'Jacqueline', 'Test Todo 2', sysdate(), false);

INSERT INTO todo(id, username, description, target_date, is_done)
VALUES(10003, 'Jacqueline', 'Test Todo 3', sysdate(), false);

CREATE SEQUENCE HIBERNATE_SEQUENCE START WITH 10004 INCREMENT BY 1;