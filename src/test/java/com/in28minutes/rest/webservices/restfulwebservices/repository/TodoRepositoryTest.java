package com.in28minutes.rest.webservices.restfulwebservices.repository;

import com.in28minutes.rest.webservices.restfulwebservices.model.Todo;
import com.in28minutes.rest.webservices.restfulwebservices.repository.TodoJpaRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { H2TestProfileJPAConfig.class }, loader = AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext
@Sql(scripts = {"classpath:data.sql"})
public class TodoRepositoryTest {

    @Resource
    private TodoJpaRepository todoRepository;
   

    @Test
    //GET
    public void testFindAllTodos() {
         
        List<Todo> todoList = todoRepository.findAll();
        
          //assert size of todolist is 3
		  assertEquals(3,todoList.size()); 
    }
    @Test
    //GET
    public void testFindTodoById() {
         
        Todo todo = todoRepository.findById(10001L).get();
        
        //System.out.println(todo.getDescription());
        //assert description matches
        assertEquals("Test Todo 1",todo.getDescription());
    }
    @Test
    //DELETE
    public void testDeleteTodo() {
         
        List<Todo> todoListOriginal = todoRepository.findAll();
		assertEquals(3,todoListOriginal.size());
		
		//delete todo
		todoRepository.deleteById((long) 10001);
		
		//assert that size of lize decreased by one
	    List<Todo> todoListDeleted = todoRepository.findAll();
		assertEquals(2,todoListDeleted.size());
    }
    @Test
    //PUT
    public void testUpdateTodo() {
         
    	Todo todo = todoRepository.findById(10002L).get();
    	
    	//assert description 
    	assertEquals("Test Todo 2",todo.getDescription());
    	
    	//make a edit to todo description
    	todo.setDescription("New Description");
    	
    	//save changes to todo
		todoRepository.save(todo);
		
		//assert todo has a new description
		assertEquals("New Description",todo.getDescription());

    }
    @Test
    //POST
    public void testCreateTodo() {  
    	
    	//create new todo
    	Todo newTodo = new Todo(-1L,"Jacqueline",
				"Test Todo 4", new Date(),false);
    	
    	//assert it does not exist yet in db
    	Exception caughtException = null;
    	try {
    		todoRepository.findById(10004L).get();
    	}
    	catch(Exception e) {
    		 caughtException = e;
    	}
    	assertEquals("java.util.NoSuchElementException: No value present",caughtException.toString());
    	
    	//save todo
		todoRepository.save(newTodo);
		Todo savedTodo = todoRepository.findById(10004L).get();
		
		//assert it now exists in db
		assertEquals("Test Todo 4", savedTodo.getDescription());
    }
}