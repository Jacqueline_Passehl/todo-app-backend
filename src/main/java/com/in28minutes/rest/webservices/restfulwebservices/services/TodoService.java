package com.in28minutes.rest.webservices.restfulwebservices.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.in28minutes.rest.webservices.restfulwebservices.model.Todo;
import com.in28minutes.rest.webservices.restfulwebservices.repository.TodoJpaRepository;


@Service //we want Spring to manage this
public class TodoService {
	
	//static list of Todos- for now this will act as our database
	private static List<Todo> todos = new ArrayList();
	private static Long idCounter = 0L;
	
	@Autowired
	private TodoJpaRepository todoJpaRepository;
	
	public List<Todo> getAllByUsername(String username){
		return  todoJpaRepository.findByUsername(username);
	}
	
	public void deleteById(long id) {
//		Todo todo = findById(id);
//		if(todo != null) {
//			todos.remove(todo);
//		}
//		System.out.println(todo);
//		return todo;
		todoJpaRepository.deleteById(id);
	}
	
	public Todo findById(long id) {
//		for(Todo todo : todos) {
//			if(todo.getId() == id) {
//				return todo;
//			}
//		}
//		return null;
		return todoJpaRepository.findById(id).get();
	}
	public Todo save(String username,Todo todo) {
//		if(todo.getId()==-1 || todo.getId() == 0 ) { //if the request is to create a new ID
//			todo.setId(++idCounter);
//			todos.add(todo);
//		}else {
//			deleteById(todo.getId());
//			todos.add(todo);
//		}
//		return todo;
		todo.setUsername(username);
		Todo updatedTodo = todoJpaRepository.save(todo);
		return updatedTodo;
	}
	public Todo create(String username,Todo todo) {
		
		todo.setUsername(username);
		Todo createdTodo = todoJpaRepository.save(todo);
		return createdTodo;
	}
}
